#include <sys/types.h>
#include <string.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <malloc.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "transfer-udp.h"

//=============================================================================================================================
//==================================================server=====================================================================
//=============================================================================================================================
typedef struct udp_server_arg{
	void (* callback)(char * addr, int port, void* msg, int len);
	int sockfd;
	pthread_t tid;
} udp_server_arg;

static void * udp_server_th(void * arg)
{
	udp_server_arg * udp_arg = arg;
	struct sockaddr_in cliaddr;
	char msg[UDP_BUF_LEN];
	int len;
	socklen_t socklen;

	for(;;){
		socklen = sizeof(cliaddr);
		len = recvfrom(udp_arg->sockfd, (void*)msg, UDP_BUF_LEN, 0, (struct sockaddr*)&cliaddr, &socklen);
		(* udp_arg->callback)(inet_ntoa(cliaddr.sin_addr), htons(cliaddr.sin_port), msg, len);
	}

	close(((udp_server_arg *)arg)->sockfd);
	free(arg);
	return NULL;
}

int udp_server_bind(int port, void (* callback)(char * addr, int port, void* msg, int len))
{
	int sockfd;
	struct sockaddr_in servaddr;
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(port);

	if(bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
	{
		return -1;
	}
	
	if(callback){
		udp_server_arg * arg = malloc(sizeof(udp_server_arg));
		arg->callback = callback;
		arg->sockfd = sockfd;
		pthread_create(&arg->tid, NULL, udp_server_th, arg);
	}
	
	return sockfd;
}
//=============================================================================================================================
//==================================================client=====================================================================
//=============================================================================================================================
static inline int udp_connect(char * addr, int port)
{
	int sockfd;
	struct sockaddr_in servaddr;

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(port);
	if(inet_pton(AF_INET, addr, &servaddr.sin_addr) <= 0)
	{
		return -1;
	}

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	
	if(connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
	{
		return -1;
	}

	return sockfd;
}

int udp_message_send(char *addr, int port, char *msg, int len)
{
	int sockfd = udp_connect(addr, port);
	if(sockfd == -1) return -1;
	len = write(sockfd, msg, len);
	close(sockfd);
	return 0;
}

int udp_broadcast(int port, void *msg, int len)
{
	static struct sockaddr_in s_addr;
	static int sock = 0;
	int addr_len;
	int yes;

	if(!sock){
		/* 创建 socket */
		if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
			return -1;
		}

		/* 设置通讯方式对广播，即本程序发送的一个消息，网络上所有主机均可以收到 */
		yes = 1;
		setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
		/* 唯一变化就是这一点了 */

		/* 设置对方地址和端口信息 */
		s_addr.sin_family = AF_INET;
		s_addr.sin_port = htons(port);
		s_addr.sin_addr.s_addr = inet_addr("255.255.255.255");
	}

	/* 发送UDP消息 */
	addr_len = sizeof(s_addr);
	return sendto(sock, msg, len, 0, (struct sockaddr *)&s_addr, addr_len);
}