#ifndef _H_TRANSFER_USER
#define _H_TRANSFER_USER

#define SERV_PORT 8888

typedef struct user_msg{
	char ip[16];
	int port;
	int times;
	int len;
//tcp_msg
	char from[32];
	char to[32];
	char msg[2048];
}user_msg;

void user_init(void);
void user_send(user_msg *umsg);
void add_user(char * user);
void remove_user(char * user);
#endif
