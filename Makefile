CFLAGS	= -Wall -W -O2 -g
LIBS	= -lpthread
# -lrt
ALL =  daemon test

all: $(ALL)

clean:
	rm -f *.o
	rm -f $(ALL)

daemon: transfer-daemon.o transfer-udp.o transfer-tcp.o transfer-users.o transfer-list.o transfer-cache.o
	cc $(CFLAGS) -o $@ $^ $(LIBS)

test: transfer-test.o transfer-lib.o
	cc $(CFLAGS) -o $@ $^ $(LIBS)
