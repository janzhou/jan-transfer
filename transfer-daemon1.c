#include "transfer-users.h"
#include <stdio.h>
#include <string.h>
#include <pthread.h>

static void * tcp_send_th(void * arg){
	char txt[1024] = "message";
	int i, id = arg;
	user_msg *umsg;
	for(i = 0; i < 10240; i++){
		umsg = malloc(sizeof(user_msg)+1024);
		sprintf(umsg->from, "user%d", id+20);
		sprintf(umsg->to, "user%d", id);
		sprintf(umsg->msg, "%s%d",txt, i);
		umsg->len = sizeof(user_msg)+1024;
		umsg->times = 3;
		user_send(umsg);
	}

	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	printf("thread %d end time: %ldsec %ldusec\n", arg, tv.tv_sec, tv.tv_usec);
}

//=============================================================================================================================
//==========================================================main===============================================================
//=============================================================================================================================
int main(void)
{
	int i;char * user;pthread_t tid;
	
	user_init();

	for(i = 20; i < 40; i++){
		user = malloc(32);
		sprintf(user, "user%d", i);
		add_user(user);
	}

	sleep(5);
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	printf("start time: %ldsec %ldusec\n", tv.tv_sec, tv.tv_usec);

//	for(i = 0; i < 20; i++){
//		pthread_create(&tid, NULL, tcp_send_th, i);
//	}

	sleep(240);
	return 0;
}
