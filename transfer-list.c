#include <malloc.h>
#include "transfer-list.h"

typedef struct list_node{
	struct list_node * next;
	struct list_node * pre;
	void * data;
}list_node;

void add_to_list(list_head * head, void* data)
{

	list_node * node = NULL;
	if(!head) return;

	node = malloc(sizeof(list_node));
	node->data = data;
	node->next = (list_node *)head;

	sem_wait(&head->left_len);
	sem_wait(&head->sem);
	node->pre = head->pre;
	((list_node*)head->pre)->next = (void *)node;
	head->pre = node;
	sem_post(&head->sem);
	sem_post(&head->list_len);
}

static inline void init_list(list_head * head, int max_length)
{
	head->next = head->pre = head;
	sem_init(&head->sem, 0, 1);
	sem_init(&head->list_len, 0, 0);
	sem_init(&head->left_len, 0, max_length);
}

list_head * creat_list(int max_length)
{
	list_head* head = malloc(sizeof(list_head));
	init_list(head, max_length);
	return head;
}

void del_list(list_head * head)
{
	if(!head) return;
	sem_destroy(&head->sem);
	free(head);
}

void* get_from_list(list_head * head)
{
	list_node * node = NULL;
	void* data = NULL;
	
	if(!head) return NULL;

	sem_wait(&head->list_len);
	sem_wait(&head->sem);
	if(head->next != head){
		node = head->next;
		node->next->pre = (void *)head;
		head->next = node->next;
	}
	sem_post(&head->sem);
	sem_post(&head->left_len);

	if(node){
		data = node->data;
		free(node);
	}
	return data;
}

void* reget_from_list(list_head * head,  void* data)
{
	list_node * node = NULL;
	void* redata = data;
	
	if(!head) return NULL;

	sem_wait(&head->sem);
	if(head->next != head){
		node = head->next;
		//detach from head
		node->next->pre = (void *)head;
		head->next = node->next;
		
		redata = node->data;
		node->data = data;
		//add to tail
		node->next = (list_node *)head;
		node->pre = head->pre;
		((list_node*)head->pre)->next = (void *)node;
		head->pre = node;
	}
	sem_post(&head->sem);

	return redata;
}

int get_length_list(list_head * head, int* plen)
{
	return sem_getvalue(&head->list_len, plen);
}

int get_leftlen_list(list_head * head, int* plen)
{
	return sem_getvalue(&head->left_len, plen);
}
