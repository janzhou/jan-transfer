#include "transfer-users.h"
#include "transfer-list.h"

#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <ctype.h>
#define Server_FIFO_Name "serv_fifo"

static void * msg_send_th(void * msg_list){
	user_msg *umsg;
	char * user;
	while((umsg = (user_msg*)get_from_list((list_head*)msg_list)) != NULL){
printf("user msg\n");
		if(umsg->times < 0){
			printf("add user %s\n", umsg->from);
			user = malloc(32);
			strcpy(user, umsg->from);
			add_user(user);
		}
		else user_send(umsg);
	}
	return NULL;
}

//=============================================================================================================================
//==========================================================main===============================================================
//=============================================================================================================================
int main(void)
{
	user_init();

	int server_fifo_fd;

	int read_res;
	user_msg *umsg = malloc(sizeof(user_msg));
	pthread_t tid;
	list_head* msg_list;

 	pthread_create(&tid, NULL, msg_send_th, msg_list = (list_head*)creat_list(20));

	mkfifo(Server_FIFO_Name,0777);
Again:	server_fifo_fd = open(Server_FIFO_Name , O_RDONLY);
	if( -1 == server_fifo_fd ){
		fprintf( stderr , "Server fifo failure\n" );
		exit(-1);
	}

	read_res = read(server_fifo_fd , &(umsg->times), sizeof(user_msg));
	if(read_res > 0){
		add_to_list(msg_list, (void*)umsg);
		umsg = malloc(sizeof(user_msg));
	}
	close(server_fifo_fd);	
 
	goto Again;	
	unlink(Server_FIFO_Name);
	return 0;
}
