#ifndef _H_TRANSFER_UDP
#define _H_TRANSFER_UDP

#define UDP_BUF_LEN 255

int udp_server_bind(int port, void (* callback)(char * addr, int port, void* msg, int len));
int udp_broadcast(int port, void *msg, int len);
int udp_message_send(char *addr, int port, char *msg, int len);

#endif
