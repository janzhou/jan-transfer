#include "transfer-udp.h"
#include "transfer-tcp.h"
#include "transfer-users.h"
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "transfer-list.h"
#include "transfer-cache.h"

//=============================================================================================================================
//==================================================local user=====================================================
//=============================================================================================================================
static cache_head* local = NULL;

void add_user(char * user)
{
	remove_cache(local, (void*)user, strcmp);
	add_cache(local, (void*)user);
}

void remove_user(char * user)
{
	remove_cache(local, (void*)user, strcmp);
}

//=============================================================================================================================
//==================================================cache remote user==========================================================
//=============================================================================================================================
typedef struct cache_user{
	char name[32];
	char ip[16];
}cache_user;

cache_head * cache = NULL;

void swap_cache_user(cache_user * user)
{
	swap_cache(cache, user);
}
//=============================================================================================================================
//==================================================udp remote user============================================================
//=============================================================================================================================
struct pkt_search_user{
	int pkt_type;
	char user[32];
};

struct pkt_search_user_answer{
	int pkt_type;
	char user[32];
};

#define PKT_SEARCH_USER 0X0001
#define PKT_SEARCH_USER_ANSWER 0X0002

static void udp_serve_callback(char * addr, int port, void* msg, int len)
{
	switch(*(int *)msg){
		case PKT_SEARCH_USER:
			if(hit_cache(local, (void*)((struct pkt_search_user*)msg)->user, strcmp)){
				((struct pkt_search_user_answer *)msg)->pkt_type = PKT_SEARCH_USER_ANSWER;
				udp_message_send(addr, SERV_PORT, msg, sizeof(struct pkt_search_user_answer));
			}
			break;
		case PKT_SEARCH_USER_ANSWER:
			{
				struct pkt_search_user_answer *search = msg;
				struct cache_user* user = malloc(sizeof(cache_user));
				strcpy(user->name, search->user);
				strcpy(user->ip, addr);
				swap_cache_user(user);
			}
			break;
	}
}

static int search_remote(char * name)
{
	struct pkt_search_user start;
	start.pkt_type = PKT_SEARCH_USER;

	strcpy(start.user, name);
	udp_broadcast(SERV_PORT, &start, sizeof(start));

	sleep(2);

	return hit_cache(cache, name, strcmp);
}
//=============================================================================================================================
//==================================================tcp used for transfer data============================================
//=============================================================================================================================
void tcp_serve_callback(tcp_msg *msg)
{
	user_msg *umsg = msg;static int numrecv = 0;
}
//=============================================================================================================================
//==================================================user send=============================================================
//=============================================================================================================================
list_head * send_list = NULL;

void user_init(void)
{
	if(0 > udp_server_bind(SERV_PORT, udp_serve_callback)) exit(0);
	if(0 > tcp_server_bind(SERV_PORT, tcp_serve_callback)) exit(0);
	send_list = creat_list(10);
	local = creat_cache(0);
	cache = creat_cache(50);
}

void user_send(user_msg *umsg)
{
	cache_user *cuser;
	umsg->port = SERV_PORT;
	umsg->times = 3;
	if(cuser = hit_cache(local, umsg->to, strcmp)){//local
	}
	if((cuser = hit_cache(cache, umsg->to, strcmp))
		|| (cuser = search_remote(umsg->to))
		|| (cuser = search_remote(umsg->to))){
		strcpy(umsg->ip, cuser->ip);
		tcp_message_send((tcp_msg*)umsg);
	}//else printf("not fond\n");
}
