#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <ctype.h>
#define Server_FIFO_Name "serv_fifo"

typedef struct lib_msg{
	int times;
	int len;
	char from[32];
	char to[32];
	char msg[2048];
}lib_msg;


char lib_user[32] = "";


static void * lib_th(void * arg)
{
	lib_msg lmsg;
	int client_fifo_fd, readlen;
	while(1){
		client_fifo_fd = open(lib_user,O_RDONLY);
		if(client_fifo_fd != -1){	
			readlen = read(client_fifo_fd , &lmsg.len, sizeof(lib_msg));			
			if(readlen > 0){
				printf("frome:%s to:%s msg:%s\n", lmsg.from, lmsg.to, lmsg.msg);
			}
		}
		close(client_fifo_fd);
	}
	unlink(lib_user);
	return NULL;
}

int trans_init(char * user)
{
	int server_fifo_fd;
	lib_msg adduser;
	int rst;

	if(mkfifo(user, 0777) == -1){
		fprintf(stderr, "Sorry, can't make %s\n", user);
		return -1;
	}

	server_fifo_fd = open(Server_FIFO_Name,O_WRONLY);
	if(server_fifo_fd == -1){
		fprintf(stderr , "Sorry, no server\n");
		return -1;
	}
	adduser.times = -1;
	sprintf(adduser.from, user);
	write(server_fifo_fd, &adduser, 64);
	close(server_fifo_fd);
 	sprintf(lib_user, user);

	return 0;
}

void trans_del(void)
{
}

void trans_send(char* to, void* msg, int len)
{
}
