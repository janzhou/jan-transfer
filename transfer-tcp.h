#ifndef _H_TRANSFER_TCP
#define _H_TRANSFER_TCP

#define MAX_TCP_BUF_LENGTH 20
#define NUM_SEND_THREAD 10
#define NUM_RECV_THREAD 10
#define NUM_SUB_THREAD 1


typedef struct tcp_msg{
	char ip[16];
	int port;
	int times;
	int len;
	char msg[1];
}tcp_msg;

int tcp_server_bind(int port, void (*callback)(tcp_msg *msg));
void tcp_message_send(tcp_msg *msg);

#endif
