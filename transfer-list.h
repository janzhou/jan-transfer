#ifndef _H_TRANSFER_LIST
#define _H_TRANSFER_LIST

#include <semaphore.h>

typedef struct list_head{
	void * next;
	void * pre;
	sem_t sem;
	sem_t list_len;
	sem_t left_len;
}list_head;

list_head * creat_list(int max_length);
void add_to_list(list_head * head, void* node);
void* get_from_list(list_head * head);
void del_list(list_head * head);
int get_length_list(list_head * head, int* plen);
int get_leftlen_list(list_head * head, int* plen);
void* reget_from_list(list_head * head,  void* data);
#endif
