#include <sys/socket.h>
#include <arpa/inet.h>
#include <malloc.h>
#include "transfer-tcp.h"
#include <string.h> 
#include <pthread.h>
#include <unistd.h>/*close*/
#include "transfer-list.h"

typedef struct tcp_pkt_start_transfer{
	int pkt_type;
	int len;
}tcp_pkt_start_transfer;

#define TCP_START_TRANSFER 0x0001
#define PKT_LEN 1024

//======================================================================================
//======================client==========================================================
//======================================================================================
static list_head * send_msg;

static inline int tcp_connect(char * addr, int port){
	struct sockaddr_in sin;
	int new_sock;

	bzero(&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);

	//创建socket初始化
	if(inet_pton(AF_INET, addr, &sin.sin_addr) != 1){
		return -1;
	}

	if((new_sock = socket(AF_INET, SOCK_STREAM, 0)) ==-1){
		return -1;
	}
	
	//连接服务器
	if(!connect(new_sock, (struct sockaddr *) &sin, sizeof(sin))){
		return new_sock;
	}
	
	return -1;
}

void tcp_message_send(tcp_msg *msg)
{
	if(msg)	add_to_list(send_msg, msg);
}

static inline void check_msg(tcp_msg* msg)
{
	if(!msg->times){
		free(msg);
	} else{
		msg->times--;
		reget_from_list(send_msg, msg);
		return;
	}
}

static void * tcp_send_th(void * arg)
{
	tcp_msg* msg = arg;
	int sockfd = -1;
	struct tcp_pkt_start_transfer start;
	start.pkt_type = TCP_START_TRANSFER;

	while(msg = (tcp_msg*)get_from_list(send_msg)){
		sockfd = tcp_connect(msg->ip, msg->port);

		if(sockfd == -1){
			check_msg(msg);
			continue;
		}

		start.len = msg->len;
		if(sizeof(start) != send(sockfd, &start, sizeof(start), 0)){
			close(sockfd);
			check_msg(msg);
			continue;
		}
		if(msg->len != send(sockfd, msg, msg->len, 0)){
			close(sockfd);
			check_msg(msg);
			continue;
		}
		close(sockfd);
		free(msg);
	}
	return NULL;
}
//======================================================================================
//======================server==========================================================
//======================================================================================
typedef struct tcp_send_arg{
	pthread_t tid;
} tcp_send_arg;

typedef struct tcp_server_arg{
	int sockfd;
	list_head * msg_list;
	pthread_t tid;
} tcp_server_arg;

typedef struct tcp_server_sub{
	void (*callback)(tcp_msg *msg);
	list_head * msg_list;
	pthread_t tid;
}tcp_server_sub;

static void * tcp_server_sub_th(void * arg)
{
	tcp_server_sub * sub_arg = arg;
	tcp_msg * msg;
	
	while(msg = get_from_list(sub_arg->msg_list)){
		(* sub_arg->callback)(msg);
		free(msg);
	}

	del_list(sub_arg->msg_list);
	free(arg);

	return NULL;
}

static void * tcp_server_th(void * arg)
{
	tcp_server_arg * tcp_arg = arg;
	struct sockaddr_in clientAdd;
	socklen_t len = sizeof(struct sockaddr_in);
	int clientfd;
	tcp_msg * msg = NULL;
	tcp_pkt_start_transfer start;

	while((clientfd = accept(tcp_arg->sockfd,(struct sockaddr *)&clientAdd, &len)) > 0){
		if(sizeof(start) == recv(clientfd, &start, sizeof(start), 0) && start.pkt_type == TCP_START_TRANSFER){
			msg = malloc(start.len);
			if(start.len != recv(clientfd, msg, start.len, 0)){
				free(msg);
				msg = NULL;				
			}
		}
		close(clientfd);

		if(msg){
			strcpy(msg->ip, inet_ntoa(clientAdd.sin_addr));
			msg->port = htons(clientAdd.sin_port);
			msg->len = start.len;
			add_to_list(tcp_arg->msg_list, msg);
			msg = NULL;
		}

		len = sizeof(struct sockaddr_in);
	}
	close(tcp_arg->sockfd);
	free(arg);
	return NULL;
}

int tcp_server_bind(int port, void (*callback)(tcp_msg *msg))
{
	int sockfd = socket(AF_INET,SOCK_STREAM,0);

	if(!send_msg) send_msg = creat_list(MAX_TCP_BUF_LENGTH);

	if(sockfd == -1){
		return -1;
	}

	struct sockaddr_in sa;
	memset(&sa,0,sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	bzero(&(sa.sin_zero),8);

	if(bind(sockfd, (struct sockaddr *)&sa, sizeof(sa))!=0){
		return -1;
	}

	if(listen(sockfd, 1)!=0){
		return -1;
	}
	tcp_msg * msg_list = creat_list(MAX_TCP_BUF_LENGTH);
	int i;
	for(i = 0; i < NUM_SEND_THREAD; i++){
		tcp_send_arg * send_arg = malloc(sizeof(tcp_send_arg));
		if(pthread_create(&send_arg->tid, NULL, tcp_send_th, send_arg)){
			free(send_arg);
			close(sockfd);
			return -1;
		}
	}
	for(i = 0; i < NUM_RECV_THREAD; i++){
		tcp_server_arg * tcp_arg = malloc(sizeof(tcp_server_arg));
		tcp_arg->msg_list = msg_list;
		tcp_arg->sockfd = sockfd;
		if(pthread_create(&tcp_arg->tid, NULL, tcp_server_th, tcp_arg)){
			free(tcp_arg);
			close(sockfd);
			return -1;
		}
	}
	for(i = 0; i < NUM_SUB_THREAD; i++){
		tcp_server_sub * sub_arg = malloc(sizeof(tcp_server_sub));
		sub_arg->msg_list = msg_list;
		sub_arg->callback = callback;
		if(pthread_create(&sub_arg->tid, NULL, tcp_server_sub_th, sub_arg)){
			free(sub_arg);
			close(sockfd);
			return -1;
		}
	}
	return sockfd;
}
